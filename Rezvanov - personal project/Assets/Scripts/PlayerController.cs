﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    public float speed = 9.0f;
    private float bounsZ = 9.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
        ConstrainPlayerPosition();
    }
    private void MovePlayer()
    {
        float verticalInput = Input.GetAxis("Vertical");
        float horizontalInput = Input.GetAxis("Horizontal");

        rb.AddForce(Vector3.forward * verticalInput * speed);
        rb.AddForce(Vector3.right * horizontalInput * speed);
    }

    private void ConstrainPlayerPosition()
    {

        if (transform.position.z < -bounsZ)
            transform.position = new Vector3(transform.position.x, transform.position.y, -bounsZ);

        if (transform.position.z > bounsZ)
            transform.position = new Vector3(transform.position.x, transform.position.y, bounsZ);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("collision with enemy");

        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp"))
        {
            Destroy(other.gameObject);
        }
    }
}
