using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDown : MonoBehaviour
{
//
    public float speed = 5f;
    private Rigidbody objectRb;
    private float positionToRemove = -11.0f;

    void Start()
    {
        objectRb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        objectRb.AddForce(Vector3.forward * -speed);
        RemoveObject();
    }

    private void RemoveObject()
    {
        if (transform.position.z < positionToRemove)
            Destroy(gameObject);
    }
//
}
