﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] prefabs;
    public GameObject powerUp;
    private float xSpawn = 16.0f;
    private float zPowerUp = 8.0f;
    private float zEnemySpawn = 12.0f;
    private int randomIndex;


    void Start()
    {
        InvokeRepeating("SpawnEnemy", 0, 2f);
        InvokeRepeating("PowerUpSpawn", 0, 3f);
    }

    void Update()
    {
        
    }

    private void SpawnEnemy()
    {
        float randomX = Random.Range(-xSpawn, xSpawn);
        randomIndex = Random.Range(0, prefabs.Length);
        Vector3 spawnPos = new Vector3(randomX, 0.6f, zEnemySpawn);

        Instantiate(prefabs[randomIndex], spawnPos, prefabs[randomIndex].transform.rotation);
    }
    /*You need to create an additional array for the bonus system 
     * come up with different bonuses besides adding life
     */
    private void PowerUpSpawn()
    {
        float randomX = Random.Range(-xSpawn, xSpawn);
        Vector3 spawnPos = new Vector3(randomX, 0.8f, zPowerUp);

        Instantiate(powerUp, spawnPos, powerUp.transform.rotation);
    }
    //system of buff
}
